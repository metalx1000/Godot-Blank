#!/bin/bash
###################################################################### 
#Copyright (C) 2021  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

#check if source directory already exists.  if so then exit
[[ -d "src" ]] && echo "src directory already exists." && exit 1

path="$PWD"
url="https://gitlab.com/metalx1000/Godot-Blank/-/archive/master/Godot-Blank-master.zip"

if [[ "$1" == "upgrade" ]];then
  wget "https://gitlab.com/metalx1000/Godot-Blank/-/raw/master/godot-blank.sh" -O $0
  exit
fi
if [[ "$1" == "" ]];then
  read -p "Please Enter a Project Name: " name
else
  name=$*
fi

[[ "$name" == "" ]] && exit

tmp="$(mktemp -d)"

wget "$url" -O $tmp/gb.zip || exit 1

unzip -d "$tmp" "$tmp/gb.zip"

mv "$tmp/Godot-Blank-master/src" .

sed -i "s/blankgamename/$name/g" src/project.godot

godot3 src/project.godot

rm -fr "$tmp"

